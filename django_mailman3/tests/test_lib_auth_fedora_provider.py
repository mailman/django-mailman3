# -*- coding: utf-8 -*-
# Copyright (C) 2017-2023 by the Free Software Foundation, Inc.
#
# This file is part of Django-Mailman3.
#
# HyperKitty is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# HyperKitty is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# Django-Mailman3.  If not, see <http://www.gnu.org/licenses/>.
from unittest.mock import patch

from django.test import RequestFactory, TestCase
from django.urls import reverse

from allauth.socialaccount.tests import setup_app

from django_mailman3.lib.auth.fedora.provider import (
    FedoraAccount, FedoraProvider)


class TestFedoraAccount(TestCase):
    """
    Test FedoraAccount social account.
    """

    def setUp(self):
        self.account = FedoraAccount('')

    def test_self_brand(self):
        self.assertEqual(self.account.get_brand(),
                         {'id': 'fedora', 'name': 'Fedora'})


class TestFedoraProvider(TestCase):
    """
    Test FedoraProvider openid authentication.
    """
    provider_id = FedoraProvider.id

    def setUp(self):
        self.app = setup_app(self.provider_id)
        self.app.provider_id = self.provider_id
        self.app.provider = "fedora"
        self.app.save()
        self.request = RequestFactory().get("/")
        self.provider = self.app.get_provider(self.request)

    def test_get_login_url(self):
        req = self.request
        login_url = self.provider.get_login_url(req)
        self.assertEqual(login_url, reverse('fedora_login'))
        login_url = self.provider.get_login_url(req, query1='value1')
        new_url = reverse('fedora_login') + '?query1=value1'
        self.assertEqual(login_url, new_url)

    def test_extract_email_addresses(self):
        emails = self.provider.extract_email_addresses(
            {"email": "testuser@example.com", "sub": "bob"})
        self.assertEqual(len(emails), 2)
        self.assertEqual(sorted([x.email for x in emails]),
                         ['bob@fedoraproject.org', 'testuser@example.com'])

    def test_server_url(self):
        mock_sp_settings = {
            "server_url": "https://id.fedoraproject.org"
        }
        with patch(
                "django_mailman3.lib.auth.fedora.provider.django_settings"
        ) as mock_settings:
            mock_settings.SOCIALACCOUNT_PROVIDERS.get.return_value = (
                mock_sp_settings
            )
            server_url = self.provider.server_url

        self.assertEqual(
            server_url,
            "https://id.fedoraproject.org/.well-known/openid-configuration"
        )

    def test_token_auth_method(self):
        mock_sp_settings = {
            "token_auth_method": "basic_auth_token"
        }
        with patch(
                "django_mailman3.lib.auth.fedora.provider.django_settings"
        ) as mock_settings:
            mock_settings.SOCIALACCOUNT_PROVIDERS.get.return_value = (
                mock_sp_settings
            )
            token_auth_method = self.provider.token_auth_method

        self.assertEqual(
            token_auth_method,
            "basic_auth_token"
        )
