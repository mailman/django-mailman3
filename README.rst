==============================
Django library for Mailman UIs
==============================

This package contains libraries and templates for Django-based interfaces
interacting with Mailman.

To use this application, add ``django_mailman3`` to the ``INSTALLED_APPS`` list
in your Django server's settings file.


Enabling Fedora authentication provider
=======================================

To enable Fedora authentication provider in Django server add ``django_mailman3.lib.auth.fedora``
to the ``INSTALLED_APPS`` and the following to
``SOCIALACCOUNT_PROVIDERS`` in server's settings file.

```
'fedora': {
    "server_url": "https://id.fedoraproject.org/",
    "token_auth_method": "client_secret_post",
}
```

You need to have your application entry in Fedora Ipsilon (authentication service for
OIDC in Fedora). To request one create a ticket in https://pagure.io/fedora-infrastructure/issues

After that access the Django application admin interface and in ``Social applications`` click
``Add Social Application`` and fill up the following:

* **Provider**: Fedora
* **Name**: <Anything works>
* **Client id**: <client_id provided by Fedora Infrastructure Team>
* **Secret key**: <client_secret provided by Fedora Infrastructure Team>
* **Sites**: <Choose the domains for which the Fedora auth will be used>

Migration from Fedora OpenID backend
====================================

Fedora is moving from ``OpenID`` to ``OpenIDConnect`` and plans to drop ``OpenID``
support in the future. Fedora ``OpenID`` support is being dropped from Mailman as
of django-mailman3 1.3.16.

If you are currently using Fedora ``OpenID``, you need to update your
``SOCIALACCOUNT_PROVIDERS`` and Django ``Social applications`` as mentioned in section
Enabling Fedora authentication provider.

Above that the existing users that are using Fedora OpenID authentication backend need
to be migrated to ``OpenIDConnect`` solution.

To do that you need to update the usernames for fedora provider from
`http://<username>.id.fedoraproject.org/` to `<username>` in `socialaccount_socialaccount`
table in hyperkitty database.

To get all the entries that need to be changed use following select above the hyperkitty
database:

`SELECT id, uid FROM socialaccount_socialaccount WHERE provider='fedora';`

To change the uid for a row use the following update:

`UPDATE socialaccount_socialaccount SET uid = '<username>' WHERE id = '<row_id_obtained_in_select>';`

Or you can use the same migration script that was used by Fedora Infrastructure
and which could be found here https://pagure.io/fedora-infra/ansible/blob/6c5a88e986eb9ee5425ec17f45adc1d058c9ee32/f/roles/mailman3/files/migrate_fedora_auth.py
(it's created for postgreSQL).


NEWS
====

1.3.16 (2024-XX-XX)
-------------------

* Remove `default_app_config` since it is no longer required in Django. (See !257)
* **BREAKING CHANGE** Update fedora auth provider to OpenID Connect (See above and #83)
* Fix a bug where user could update their username to invalid values. (Fixes #76)
* When no timezone is set, Profile objects by default now use the time zone defined in ``settings.TIME_ZONE``.
* Added template with our style for accounts/confirm-email/ page. (Fixes #86)
* We now support/require django-allauth>=65.4.

1.3.15 (2024-06-29)
-------------------

* Use the source-includes directive in pyproject.toml instead of
  includes to include files that aren't required for wheels and
  doesn't need to be copied to site-packages. (Fixes #80)

1.3.14 (2024-06-23)
-------------------

**This release has _also_ been yanked from PyPI because of bad build**

* Fix the pyproject.toml metadata for packaging.


1.3.13 (2024-06-22)
-------------------

**This release has been yanked from PyPI due to bad build**.

* Fix social account buttons to work with providers other than oauth with
  multiple sub-providers.  (Fixes #77)
* Format date and time in Django DATETIME_FORMAT (Fixes #78)


1.3.12 (2024-03-07)
-------------------

* Fix the templates to work with "django-allauth>0.58.0". (See Postorius#590)
* More template fixes and require django-allauth>=0.59.0. This requires
  django.contrib.humanize in INSTALLED_APPS.
* Add a new ``SOCIALACCOUNT_ADAPTER`` which allows for signups using Social
  auth, even when the signups are disabled for regular logins. (See #71)
* Add support Django 5.0
* Drop support for Django < 4.2 since 4.0.x-4.1.x is now EOL.


1.3.11 (2023-10-22)
-------------------

* Added a migration to clear the django_sessions table to avoid Mailman Web
  #16.  This will remove user's login data and require them to log in again.
  (See !195)


1.3.10 (2023-10-21)
-------------------

Configuration
-------------

* **BREAKING CHANGE**: ``django_mailman3`` now requires django_allauth>=0.56.
  This requires the addition of ``allauth.account.middleware.AccountMiddleware``
  to ``MIDDLEWARE`` in your Django settings.  If your installation uses settings
  from ``mailman-web`` as `here <https://docs.mailman3.org/en/latest/install/virtualenv.html#initial-configuration>`_,
  upgrading mailman-web to 0.0.7 will do this.

Other Changes
-------------

* Add support for Django 4.2
* Migrate to Bootstrap 5. (See !188)
* A11y: Group related radio buttons and Multi-CheckboxInput. (Fixes #65)
* Add social account buttons to the Sign Up page. (Fixes #67)
* Remove real name requirement from user profile (Fixes #51)



1.3.9 (2022-01-04)
------------------

* Add support for Python 3.11.

1.3.8 (2022-10-22)
------------------
* Add support for Django 4.0 and 4.1 (Fixes #55)
* Add support for Python 3.10 (See !153)
* Fix the Fedora socialaccount provider. (Fixes #50)
* Add pagination only when there are more than one pages. (Fixes #58)
* Use Pytest as the test runner.

1.3.7 (2021-09-02)
------------------

* Set the minimum required version of ``mailmanclient`` in setup.py.
  (Fixes #46)

1.3.6 (2021-08-31)
------------------
* ``django_mailman3.lib.mailman.get_mailman_client()`` now supports
  Mailmanclient request hooks to be added using a new
  ``@mailmanclient_request_hook`` decorator.
* Scrubber now removes null bytes from the scrubbed message body.
* Update the Display Name of a user and it's associated addresses in Mailman
  when the display name is updated in Django.
* Sync a Django user's email address to Core even if it is not verified.
* Add an allauth account adapter to disable signups.
* Add support for Django 3.2.

1.3.5 (2021-01-15)
------------------
* Add a new method get_django_user to return Django User model. (See !99)
* Add ``delete_archives`` field to ``mailinglist_deleted`` Signal.
* Replaced deprecated ``ugettexy_lazy`` with ``gettext_lazy``. (Closes #37)


1.3.4 (2020-06-05)
------------------
* Fix a bug caused by bumping to Mailman API 3.1 in version 1.3.3 which
  resulted in 404 errors for some users. (Closes #35)


1.3.3 (2020-06-01)
------------------

- Hide "Account Connections" tab in accounts if no social account providers are
  installed. (See !54)
- Use bold font for form labels (See !82)
- Update a user's preferred_address in Mailman Core when a user updates their
  primary address in Profile. (Closes #32)
- Use Mailman's API version 3.1 to get Hex UUIDs instead of integer.
- Caught a LookupError when scrubbing an attachment with an unknown charset.
  (Closes #12)
- Properly scrub the content of message/rfc822 parts.  (Closes #34)

License
=======

Django-mailman is licensed under the
`GPL v3.0 <http://www.gnu.org/licenses/gpl-3.0.html>`_

Copyright (C) 2017-2020 by the Free Software Foundation, Inc.
