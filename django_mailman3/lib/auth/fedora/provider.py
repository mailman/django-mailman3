# -*- coding: utf-8 -*-
# Copyright (C) 2012-2023 by the Free Software Foundation, Inc.
#
# This file is part of Django-Mailman.
#
# HyperKitty is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# HyperKitty is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# Django-Mailman.  If not, see <http://www.gnu.org/licenses/>.
#
# Author: Aurelien Bompard <abompard@fedoraproject.org>
#
import logging

from django.conf import settings as django_settings

from allauth.account.models import EmailAddress
from allauth.socialaccount import providers
from allauth.socialaccount.providers.base import ProviderAccount
from allauth.socialaccount.providers.oauth2.provider import OAuth2Provider


_log = logging.getLogger(__name__)


class FedoraAccount(ProviderAccount):

    def get_brand(self):
        return dict(id='fedora', name='Fedora')

    def to_str(self):
        return self.account.extra_data.get("preferred_username")


class FedoraProvider(OAuth2Provider):
    id = 'fedora'
    name = 'Fedora'
    account_class = FedoraAccount

    @property
    def settings(self):
        if not hasattr(self, "_settings"):
            self._settings = django_settings.SOCIALACCOUNT_PROVIDERS.get(
                self.id
            )
        return self._settings

    @property
    def server_url(self):
        url = self.settings["server_url"]
        return self.wk_server_url(url)

    def wk_server_url(self, url):
        well_known_uri = "/.well-known/openid-configuration"
        if not url.endswith(well_known_uri):
            url += well_known_uri
        return url

    @property
    def token_auth_method(self):
        return self.settings.get("token_auth_method")

    def get_default_scope(self):
        return ["openid", "profile", "email"]

    def extract_uid(self, data):
        return str(data["sub"])

    def extract_common_fields(self, data):
        return dict(
            email=data.get("email"),
            username=data.get("preferred_username"),
            name=data.get("name"),
            user_id=data.get("user_id"),
            picture=data.get("picture"),
            zoneinfo=data.get("zoneinfo"),
        )

    def extract_email_addresses(self, data):
        """
        For example:

        [EmailAddress(email='john@doe.org',
                      verified=True,
                      primary=True)]
        """
        ret = []
        primary_email = data.get("email")
        if primary_email:
            # It would be added by cleanup_email_addresses(), but we add it
            # here to mark it as verified.
            ret.append(EmailAddress(
                email=primary_email, verified=True, primary=True))
        # Add the email alias provided by the Fedora project.
        ret.append(EmailAddress(
            email='%s@fedoraproject.org' % self.extract_uid(data),
            verified=True, primary=False))
        return ret


providers.registry.register(FedoraProvider)
